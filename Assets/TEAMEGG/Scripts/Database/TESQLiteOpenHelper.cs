﻿using System;
using System.IO;
using UnityEngine;

using SQLite4Unity3d;

namespace TEAMEGG
{

    public abstract class TESQLiteOpenHelper
    {
        private SQLiteConnection _sqliteConnection;

        /// <summary>
        /// DB名 
        /// </summary>
        private string _dbName;

        /// <summary>
        /// 空のDBファイル 
        /// </summary>
        private static readonly string EmptyDatabase = "empty_database.db";
        /// <summary>
        /// 内部管理バージョン用テーブル名 
        /// </summary>
        private string _versionTableName;
        private static readonly int INTERNAL_VERSION_ID = 1;

        public TESQLiteOpenHelper(string dataBaseName, int version)
        {
            // バージョン正常チェック
            if (version < 1)
            {
                throw new ArgumentException("the DB version is invalid. version must be grather than 1");
            }

            this._versionTableName = typeof(NTInternalDbVersion).Name;

            this._dbName = dataBaseName;

            string dbPath = Path.Combine(TEFileUtils.GetDatabasePath(), _dbName);

            // DBファイルチェック
            if (!File.Exists(dbPath))
            {
                // DBファイル生成
                createDatabaseFile(dbPath);
            }

            _sqliteConnection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);

            checkVersion(version);
        }

        /// <summary>
        /// DBファイルを生成します。
        /// </summary>
        private void createDatabaseFile(string dbPath)
        {
            string streamingAssetsPath = Path.Combine(TEFileUtils.GetStreamingAssetsPath(), EmptyDatabase);

            Directory.CreateDirectory(TEFileUtils.GetDatabasePath());

            if (streamingAssetsPath.Contains("file://"))
            {
                // Android
                WWW loadDb = new WWW(streamingAssetsPath);
                while (!loadDb.isDone)
                {
                    // Wait load file.
                }

                if (!String.IsNullOrEmpty(loadDb.error))
                {
                    throw new FileNotFoundException("the DB file doesn't exist in the StreamingAssets Folder.  " + streamingAssetsPath);
                }

                File.WriteAllBytes(dbPath, loadDb.bytes);
            }
            else
            {
                // Others
                if (!File.Exists(streamingAssetsPath))
                {
                    throw new FileNotFoundException("the DB file doesn't exist in the StreamingAssets Folder.  " + streamingAssetsPath);
                }

                File.Copy(streamingAssetsPath, dbPath);
            }
        }

        private void createTable()
        {
            // テーブル生成通知
            onCreate(_sqliteConnection);
            // バージョン管理テーブル生成
            _sqliteConnection.CreateTable<NTInternalDbVersion>();
        }

        private void checkVersion(int newVersion)
        {
            int version = getVersion();

            if (version != newVersion)
            {
                _sqliteConnection.BeginTransaction();
                try
                {
                    if (version == 0)
                    {
                        createTable();
                    }
                    else
                    {
                        if (version < newVersion)
                        {
                            onUpdate(_sqliteConnection, version, newVersion);
                        }
                        else
                        {
                            throw new ArgumentException("the version od the DB is downgrad.");
                        }
                    }
                    setVersion(newVersion);

                    _sqliteConnection.Commit();
                }
                finally
                {
                    // トランザクション終了処理として呼び出し。
                    // Commit()後のRollbackはスルーされるため問題なし
                    _sqliteConnection.Rollback();
                }
            }
        }


        private int getVersion()
        {
            // DB未登録
            if (_sqliteConnection.GetTableInfo(_versionTableName).Count == 0)
            {
                return 0;
            }

            return _sqliteConnection.Table<NTInternalDbVersion>().Where(record => (record.Id == INTERNAL_VERSION_ID)).FirstOrDefault().Version;
        }


        private void setVersion(int version)
        {
            int ret = _sqliteConnection.Update(new NTInternalDbVersion { Id = INTERNAL_VERSION_ID, Version = version });

            if (ret <= 0)
            {
                ret = _sqliteConnection.Insert(new NTInternalDbVersion { Id = INTERNAL_VERSION_ID, Version = version });
            }
        }

        /// <summary>
        /// DB生成通知
        /// </summary>
        public abstract void onCreate(SQLiteConnection connection);


        /// <summary>
        /// DB更新通知
        /// </summary>
        public abstract void onUpdate(SQLiteConnection connection, int oldVersion, int newVersion);

        public SQLiteConnection getSQLiteConnection()
        {
            return _sqliteConnection;
        }
    }

    public class NTInternalDbVersion
    {
        [PrimaryKey]
        public int Id { get; set; }
        public int Version { get; set; }
    }
}
