﻿using System.IO;

using UnityEngine;

namespace TEAMEGG
{
    public class TEFileUtils : MonoBehaviour
    {
        private static readonly string UNITY_DATA_PATH = Application.persistentDataPath;
        private static readonly string UNITY_STREAMING_ASSETS_PATH = Application.streamingAssetsPath;

        private static readonly string TEAMEGG_ROOT_DIR = "TEAMEGG";
        private static readonly string DATABASE_DIR = "database";

        public static string GetStreamingAssetsPath()
        {
            return Combine(UNITY_STREAMING_ASSETS_PATH, TEAMEGG_ROOT_DIR);
        }

        public static string GetDatabasePath()
        {
            return Combine(UNITY_DATA_PATH, TEAMEGG_ROOT_DIR, DATABASE_DIR);
        }

        private static string Combine(params string[] paths)
        {
            if (paths.Length == 0)
            {
                return "";
            }

            if (paths.Length < 2)
            {
                return paths[0];
            }

            string path = paths[0];
            for (int i = 1; i < paths.Length; i++)
            {
                path = Path.Combine(path, paths[i]);
            }

            return path;
        }
    }
}
