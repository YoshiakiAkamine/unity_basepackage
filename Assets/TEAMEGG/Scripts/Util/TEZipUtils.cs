﻿using System;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;


namespace TEAMEGG
{
    public static class NTZipUtils
    {

        /// <summary>
        /// zip内のエントリー数を取得
        /// </summary>
        /// <para>
        /// zipファイル内にあるエントリー（ファイル）数を取得します。
        /// </para>
        /// <param name="zip">zipファイルのバッファ</param>
        /// <returns>エントリー数</returns>
        public static long ZipEntryCount(byte[] zip)
        {
            if (zip == null)
            {
                return 0;
            }
            ZipFile zf = new ZipFile(new MemoryStream(zip));
            return zf.Count;
        }

        /// <summary>
        /// zipファイルの解凍
        /// </summary>
        /// <para>
        /// zipファイルを解凍します。解凍先ディレクトリは zipFilePath + "_unzip” になります。
        /// </para>
        /// <param name="zipFilePath">zipファイルのパス</param>
        /// <returns>zipファイルの解凍先ディレクトリパス</returns>
        public static string Unzip(string zipFilePath)
        {
            string unzipDirectoryPath = zipFilePath + "_unzip";
            Unzip(zipFilePath, unzipDirectoryPath);
            return unzipDirectoryPath;
        }

        /// <summary>
        /// zipファイルの解凍
        /// </summary>
        /// <para>
        /// zipファイルを解凍します。解凍先ディレクトリはunzipDirectoryPathで指定した場所になります。
        /// </para>
        /// <param name="zipFilePath">zipファイルのパス</param>
        /// <param name="unzipDirectoryPath">解凍先ディレクトリパス</param>
		public static void Unzip(string zipFilePath, string unzipDirectoryPath)
        {
            // zipファイルのバイナリ取得
            byte[] zipBytes = File.ReadAllBytes(zipFilePath);

            // 解凍ディレクトリを作成し直し. zipファイル名と同じディレクトリ名にする.
            if (Directory.Exists(unzipDirectoryPath))
            {
                Directory.Delete(unzipDirectoryPath, true);
            }
            Directory.CreateDirectory(unzipDirectoryPath);

            // 解凍ファイルを保存
            UnzipOnMemory(zipBytes, (string fileName, byte[] unzipBytes) =>
            {
                string filePath = Path.Combine(unzipDirectoryPath, fileName);
                string directoryPath = Path.GetDirectoryName(filePath);
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }
                File.WriteAllBytes(filePath, unzipBytes);
            });
        }

        /// <summary>
        /// zipファイルの解凍
        /// </summary>
        /// <para>
        /// zipファイルをメモリ上で解凍します。
        /// </para>
        /// <param name="zip">zipファイルのバッファ</param>
        /// <param name="partCompletion">zip内のファイル１つずつを解凍するたびにコールします。引数は(ファイル名<string>, 解凍ファイルのバッファ<byte[]>)になります。</param>
        public static void UnzipOnMemory(byte[] zip, Action<string, byte[]> partCompletion)
        {
            ZipInputStream zis = new ZipInputStream(new MemoryStream(zip));
            ZipEntry entry;
            while ((entry = zis.GetNextEntry()) != null)
            {
                if (entry.IsDirectory)
                {
                    continue;
                }

                if (entry.Name.Length > 0)
                {
                    MemoryStream output = new MemoryStream();

                    byte[] buffer = new byte[512];
                    int size = 0;
                    while ((size = zis.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        output.Write(buffer, 0, size);
                    }

                    byte[] unzipBytes = output.ToArray();
                    output.Close();

                    // 1ファイルずつ通知
                    if (partCompletion != null)
                    {
                        partCompletion(entry.Name, unzipBytes);
                    }
                }

            }

            zis.Close();
        }

    }
}