﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TEAMEGG
{


    public class WebTest : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {
            StartCoroutine(startRequest());
        }


        private IEnumerator startRequest()
        {

            TEStringWebRequest vFormatRequest = new TEStringWebRequest(this, "http://www.yahoo.co.jp/");
            vFormatRequest.RequestSuccessedCallback = delegate (string data)
            {

                Debug.Log("success to fetch palette");
                Debug.Log("data=" + data);
            };
            vFormatRequest.RequestFailuredCallback = delegate ()
            {
                Debug.Log("failure to fetch URL:");
            };
            vFormatRequest.RequestCanceledCallback = delegate ()
            {
                // キャンセル処理対応
            };
            vFormatRequest.RequestTimeoutedCallback = delegate ()
            {
                Debug.Log("timeout to fetch palette URL:");
            };

			vFormatRequest.StartRequest();
            yield return 0;
        }
    }
}