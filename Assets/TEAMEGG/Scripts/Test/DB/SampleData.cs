﻿using SQLite4Unity3d;

namespace TEAMEGG
{
    public class SampleData
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }

    }
}