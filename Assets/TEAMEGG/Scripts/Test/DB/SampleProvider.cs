﻿using System;
using System.Runtime.CompilerServices;
using SQLite4Unity3d;

namespace TEAMEGG
{
    public class SampleProvider
    {
        private SampleDatabase _satelliteDatabase;

        public SampleProvider()
        {
            _satelliteDatabase = new SampleDatabase();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Insert(String name)
        {
            SQLiteConnection connection = _satelliteDatabase.getSQLiteConnection();

            SampleData sampleData = new SampleData();
            sampleData.Name = name;
            sampleData.Value = name.GetHashCode();

            try
            {
                connection.BeginTransaction(); // トランザクション開始
                connection.Insert(sampleData);
                connection.Commit(); // 成功
            }
            catch (Exception)
            {
            }
            finally
            {
                try
                {
                    connection.Rollback(); // トランザクション終了
                }
                catch (Exception)
                {
                }
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int Find(String name)
        {

            SQLiteConnection connection = _satelliteDatabase.getSQLiteConnection();

            SampleData imageData = null;

            try
            {
                connection.BeginTransaction(); // トランザクション開始
                imageData = connection.Table<SampleData>()
                .Where(record =>
                    (name.Equals(record.Name)))
                .FirstOrDefault();
                connection.Commit(); // 成功

            }
            catch (Exception)
            {
                // 例外発生
            }
            finally
            {
                try
                {
                    connection.Rollback(); // トランザクション終了
                }
                catch (Exception)
                {
                }
            }

            if (imageData == null)
            {
                return 0;
            }
            return imageData.Value;
        }

        public int Size()
        {
            SQLiteConnection connection = _satelliteDatabase.getSQLiteConnection();

            return connection.Table<SampleData>().Count();
        }

    }
}