﻿using UnityEngine;

namespace TEAMEGG
{

    public class DBTest : MonoBehaviour
    {
        private readonly string testA = "TEST_A";
        private readonly string testB = "TEST_B";

        void Start()
        {

            bool hasData = false;

            SampleProvider provider = new SampleProvider();

            int value = provider.Find(testA);

            if (value != 0)
            {
                hasData = true;
            }

            if (!hasData)
            {
                provider.Insert(testA);
                provider.Insert(testB);
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}