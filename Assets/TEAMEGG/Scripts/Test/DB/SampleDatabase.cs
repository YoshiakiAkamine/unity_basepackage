﻿using SQLite4Unity3d;

namespace TEAMEGG
{
    public class SampleDatabase : TESQLiteOpenHelper
    {

        private static readonly string DB_NAME = "sample.db";
        private static readonly int DB_VERSION = 1;

        public SampleDatabase() : base(DB_NAME, DB_VERSION)
        {

        }

        public override void onCreate(SQLiteConnection connection)
        {
            connection.CreateTable<SampleData>();
        }

        public override void onUpdate(SQLiteConnection connection, int oldVersion, int newVersion)
        {
            // droptable対応
            connection.DropTable<SampleData>();
            connection.CreateTable<SampleData>();
        }

    }
}
