﻿using System.IO;
using UnityEngine.Networking;

namespace TEAMEGG
{
    public class TEDownloadHandler : DownloadHandlerScript
    {
        private MemoryStream _memoryStream;
        private byte[] _data;

        public TEDownloadHandler() : base()
        {
            _memoryStream = new MemoryStream();
        }

        protected override bool ReceiveData(byte[] data, int dataLength)
        {
            if (data == null || data.Length < 1)
            {

                return false;
            }

            _memoryStream.Write(data, 0, dataLength);
            return true;
        }

        protected override byte[] GetData()
        {
            return _data;
        }

        protected override void CompleteContent()
        {
            _data = _memoryStream.ToArray();
            _memoryStream.Close();
            _memoryStream.Dispose();
        }
    }
}