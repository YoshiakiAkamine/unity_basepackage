﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TEAMEGG
{

    public class TEStringWebRequest : TEAbstractWebRequest
    {
        public Action<string> RequestSuccessedCallback { private get; set; }

        public TEStringWebRequest(MonoBehaviour monoBehaviour, string url)
        : base(monoBehaviour, url)
        {
            RequestSuccessedCallback = delegate { };
            SetDownloadHandler(new TEDownloadHandler());
        }

        public override void OnRequestSuccess(byte[] data)
        {

            RequestSuccessedCallback(System.Text.Encoding.UTF8.GetString(data));
        }
    }
}