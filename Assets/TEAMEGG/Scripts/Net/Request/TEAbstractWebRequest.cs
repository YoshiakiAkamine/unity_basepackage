﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using ICSharpCode.SharpZipLib.GZip;
using UnityEngine;
using UnityEngine.Networking;

namespace TEAMEGG
{

    public abstract class TEAbstractWebRequest : MonoBehaviour
    {
        private MonoBehaviour _monoBehaviour;

        protected UnityWebRequest _webRequest;

        private bool _isRequested = false;
        private bool _isCanceled = false;

        public double Timeout { get; set; }
        public bool IsDone
        {
            get
            {
                return _webRequest == null ? false : _webRequest.isDone;
            }
        }

        public Action RequestFailuredCallback { private get; set; }
        public Action RequestTimeoutedCallback { private get; set; }
        public Action RequestCanceledCallback { private get; set; }

        private static readonly double DEFAULT_TIMEOUT = 20.0;

        public TEAbstractWebRequest(MonoBehaviour monoBehaviour, string url)
        {
            _monoBehaviour = monoBehaviour;

            Timeout = DEFAULT_TIMEOUT;

            _webRequest = UnityWebRequest.Get(url);

            RequestFailuredCallback = delegate { };
            RequestTimeoutedCallback = delegate { };
            RequestCanceledCallback = delegate { };
        }

        public void SetDownloadHandler(DownloadHandler downloadHandler)
        {
            _webRequest.downloadHandler = downloadHandler;
        }

        public bool StartRequest()
        {
            if (!_isRequested)
            {
                _monoBehaviour.StartCoroutine(Send());
                _isRequested = true;
                return true;
            }

            return false;
        }

        private IEnumerator Send()
        {

            var timeLimit = Time.realtimeSinceStartup + Timeout;

            _webRequest.Send();


            // 通信処理の完了orキャンセルorタイムアウトチェック
            while (!_isCanceled && Time.realtimeSinceStartup < timeLimit && !_webRequest.isDone)
            {
                yield return 0;
            }

            // 通信処理キャンセル
            if (_isCanceled)
            {
                _webRequest.Abort();

                RequestCanceledCallback();

                _webRequest.Dispose();
                yield break;
            }

            // 通信タイムアウト
            if (Time.realtimeSinceStartup >= timeLimit)
            {
                RequestTimeoutedCallback();

                _webRequest.Dispose();
                yield break;
            }

            // 通信完了
            if (_webRequest.isDone)
            {

                if (_webRequest.isError)
                {
                    // エラー
                    RequestFailuredCallback();
                }
                else
                {
                    OnRequestSuccess(getRequestData());
                }


            }
        }

        public byte[] getRequestData()
        {
            string encoding = _webRequest.GetResponseHeader("Content-Encoding");

            byte[] data = _webRequest.downloadHandler.data;

            if (string.IsNullOrEmpty(encoding))
            {
                return data;
            }

            return Ungzip(data);
        }


        public static byte[] Ungzip(byte[] zip)
        {
            byte[] unzipData;
            GZipInputStream zipStream = new GZipInputStream(new MemoryStream(zip));
            MemoryStream memoryStream = new MemoryStream();

            int buffSize = 512;
            byte[] outBuffer = new byte[512];
            while (true)
            {
                buffSize = zipStream.Read(outBuffer, 0, buffSize);
                if (buffSize > 0)
                {
                    memoryStream.Write(outBuffer, 0, buffSize);
                }
                else
                {
                    break;
                }
            }

            unzipData = memoryStream.ToArray();
            memoryStream.Close();
            zipStream.Close();

            return unzipData;
        }


        public abstract void OnRequestSuccess(byte[] data);

    }
}