﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TEAMEGG
{

    public class TEByteWebRequest : TEAbstractWebRequest
    {
        public Action<byte[]> RequestSuccessedCallback { private get; set; }

        public TEByteWebRequest(MonoBehaviour monoBehaviour, string url)
        : base(monoBehaviour, url)
        {
            RequestSuccessedCallback = delegate { };
            SetDownloadHandler(new TEDownloadHandler());
        }

        public override void OnRequestSuccess(byte[] data)
        {
            RequestSuccessedCallback(data);
        }
    }
}